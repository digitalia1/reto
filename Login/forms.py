from django import forms
from django.contrib.auth import authenticate, get_user_model
from home.models import UserProfile


class UserLoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-user',
                                                             'placeholder': 'User or Email'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control form-control-user',
                                                                 'placeholder': 'Password'}))

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            u = authenticate(username=username, password=password)

            if not u:
                raise forms.ValidationError('User Does Not Exist')
            
            if not u.check_password(password):
                raise forms.ValidationError('Incorrect Password')
        
        return super(UserLoginForm, self).clean()


user = get_user_model()


class UserRegisterForm(forms.ModelForm):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-user',
                                                               'placeholder': 'First Name'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-user',
                                                              'placeholder': 'Last Name'}))
    email = forms.EmailField(label='Email Address',
                             widget=forms.TextInput(attrs={'class': 'form-control form-control-user',
                                                           'placeholder': 'Email Address'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control form-control-user',
                                                                 'placeholder': 'Password'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control form-control-user',
                                                                  'placeholder': 'Repeat Password'}))
    chat_gpt_api = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control form-control-user',
                                                                 'placeholder': 'Chat GPT API Key'}))

    class Meta:
        model = user
        fields = [
            'email',
            'password',
            'password2',
        ]

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')
        chat_gpt_api = self.cleaned_data.get('chat_gpt_api')

        if password != password2:
            raise forms.ValidationError("Passwords Don't Match")

        email_qs = user.objects.filter(email=email)
        if email_qs.exists():
            raise forms.ValidationError("Email Already Exists")

        chatgpt_qs = UserProfile.objects.filter(chat_gpt_api=chat_gpt_api)
        if chatgpt_qs.exists():
            raise forms.ValidationError("ChatGPT API KEY Already Exists")

        return super(UserRegisterForm, self).clean()
