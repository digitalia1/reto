from django.shortcuts import render, redirect
from django.urls import reverse
from .forms import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from home.models import UserProfile
from home.forms import UploadCSVForm


def handle_uploaded_file(f):
    with open("media/filename.csv", "wb+") as destination:
        for chunk in f.chunks():
            destination.write(chunk)


@login_required(login_url='login/')
def home(request):
    data_profile = UserProfile.objects.get(user=request.user)
    api_key = data_profile.chat_gpt_api

    if request.method == 'POST':
        form = UploadCSVForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(request.FILES["file"])
            return redirect(reverse('csv_information'))
    else:
        form = UploadCSVForm()

    context = {
        'api_key': api_key,
        'form': form,
    }
    return render(request, "home.html", context)


def login_view(request):
    next = request.GET.get('next')
    form = UserLoginForm(request.POST or None)
    if form.is_valid():
        username = form.cleaned_data.get('username')
        password = form.cleaned_data.get('password')
        user = authenticate(username=username, password=password)
        login(request, user)
        if next:
            return redirect(next)
        return redirect('/')

    context = {
        'form': form,
    }
    return render(request, "login.html", context)


def register_view(request):
    form = UserRegisterForm(request.POST or None)
    if form.is_valid():
        u = form.save(commit=False)
        username = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        first_name = form.cleaned_data.get('first_name')
        last_name = form.cleaned_data.get('last_name')
        chat_gpt_api = form.cleaned_data.get('chat_gpt_api')

        u.username = username
        u.set_password(password)
        u.first_name = first_name
        u.last_name = last_name
        u.save()
        UserProfile.objects.get_or_create(user=u,
                                          defaults={"chat_gpt_api": chat_gpt_api})
        new_user = authenticate(username=username, password=password)
        login(request, new_user)
        return redirect('/')

    context = {
        'form': form,
    }
    return render(request, "signup.html", context)


def logout_view(request):
    logout(request)
    return redirect('/')
