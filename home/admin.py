from django.contrib import admin
from home.models import UserProfile


class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['__str__', 'chat_gpt_api']


admin.site.register(UserProfile, UserProfileAdmin)
