from django import forms


class UploadCSVForm(forms.Form):
    file = forms.FileField()

    def clean(self, *args, **kwargs):
        file = self.cleaned_data.get('file')
        if not file.name.endswith('.csv'):
            raise forms.ValidationError('Unsupported Extension')

        return super(UploadCSVForm, self).clean()
