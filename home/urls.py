from django.urls import path
from home import views


urlpatterns = [
    path('csv_information/', views.csv_information, name="csv_information"),
]
