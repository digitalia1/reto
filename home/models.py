# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE,
                                verbose_name='user', related_name='profile')
    chat_gpt_api = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.user.username

    class Meta:
        db_table = 'user_profile'
