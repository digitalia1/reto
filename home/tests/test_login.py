from home.models import UserProfile
from home.views import chat
from django.core.management import call_command
from django.urls import reverse
import pytest


@pytest.fixture(scope='session')
def django_db_setup(django_db_setup, django_db_blocker):
    with django_db_blocker.unblock():
        call_command('loaddata', 'demo_usuarios')


@pytest.mark.django_db
def test_unauthorized(client):
    url = reverse('home')
    response = client.get(url)
    assert response.status_code == 302


@pytest.mark.django_db
def test_auth_view(client):
    user = UserProfile.objects.get(user__username='demo1')
    p = user.user.email
    client.login(username=user.user.username, password=p)
    url = reverse('home')
    response = client.get(url)
    assert response.status_code == 200


@pytest.mark.django_db
def test_login_view(client):
    user = UserProfile.objects.get(user__username='demo1')
    p = user.user.email
    form_data = {
        "username": user.user.username,
        "password": p
    }
    url = reverse("login")
    response = client.get(url)
    assert response.status_code == 200

    response = client.post(url, data=form_data)
    assert response.status_code == 302

    url += "?next=/"
    response = client.post(url, data=form_data)
    assert response.status_code == 302

    client.login(username=user.user.username, password=p)
    response = client.get(url, allow_redirects=True)
    assert response.status_code == 200


@pytest.mark.django_db
def test_register_view(client):
    email = "leo@asdf.com"
    form_data = {
        "username": email,
        "email": email,
        "password": "password",
        "password2": "password",
        "first_name": "Leonardo",
        "last_name": "Fernandez",
        "chat_gpt_api": "123"
    }
    url = reverse("register")
    response = client.get(url)
    assert response.status_code == 200

    response = client.post(url, data=form_data)
    assert response.status_code == 302

    response = client.post(url, data=form_data, allow_redirects=True)
    assert response.status_code == 200


@pytest.mark.django_db
def test_logout_view(client):
    user = UserProfile.objects.get(user__username='demo1')
    p = user.user.email

    url = reverse("logout")
    client.login(username=user.user.username, password=p)
    response = client.get(url, allow_redirects=True)
    assert response.status_code == 302

@pytest.mark.django_db
def test_csv_information(client):
    user = UserProfile.objects.get(user__username='demo1')
    p = user.user.email

    url = reverse("csv_information")
    client.login(username=user.user.username, password=p)
    response = client.get(url, allow_redirects=True)
    assert response.status_code == 200

@pytest.mark.django_db
def test_chat(client):
    api_key = "123"
    input_text = "Hola"
    r = chat(api_key, input_text)
    assert r == "Incorrect API key provided"

