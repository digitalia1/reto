# -*- coding: utf-8 -*-
from django.shortcuts import render
from home.models import UserProfile
import openai


def chat(api_key, input_text):
    try:
        response = openai.Completion.create(
            engine="gpt-3.5-turbo-instruct",  # Motor de ChatGPT
            prompt=input_text,
            max_tokens=20,  # Máximo número de tokens en la respuesta
            api_key=api_key
        )
        answer = response.choices[0].text.strip()
    except openai.error.AuthenticationError:
        answer = "Incorrect API key provided"
    except openai.error.RateLimitError:
        answer = "Rate limit reached"

    return answer


def csv_information(request):
    rows = []
    with open("media/filename.csv", "r") as f:
        headers = f.readline()
        headers = headers.replace('\n', '')
        for row in f:
            rows.append(row)

    up = UserProfile.objects.get(user=request.user)
    k = up.chat_gpt_api

    m = "Hola, tengo un archivo CSV con varias columnas"
    chat(k, m)
    messages = []
    for h in headers.split(","):
        message = "Dame solo un ejemplo de '{}'".format(h)
        answer = chat(k, message)
        messages.append("Para la columna {} un ejemplo es {}".format(h, answer))

    context = {
        'headers': headers,
        'messages': messages
    }
    return render(request, "csv_information.html", context)
