# Reto



## Instalar Dependencias

Se instala mediante PIP

```console
pip install -r requirements.txt 
```

## Correr pruebas unitarias
Se utilizó pytest y coverage, para generar el reporte en HTML
```
pytest --cov
coverage html
```
El reporte se almacenará en el directorio "htmlcov"

## Levantar el entorno

```console
python manage.py runserver 8000 
```
